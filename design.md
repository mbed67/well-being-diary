# Well-Being Diary



## Personas

* User doesn't have a structure
* User is lonely
* User wants to develop a new habit
* User wants to remember positive things



## Goals

* New habits - forming in a passive, slow way, by mindfully observing behavior
  * seeing the positive feelings and mental states of these new habits (this will help maintain them)
  * remembering motivations
    * for example seeing the inter-being (connection) with others
* Reminding the users of a daily schedule (structure)
  * this structure may be such that the user can choose between different alternatives
  * the goal is to support the user, not to have a strict schedule nessacarily
  * This structure can maybe be built from the habits
* Being able to see previous good instances for a question
  * For examples of how entries can be formatted/written
  * For gratitude
  * For inspiration
* Remembering things that are important and recurring
* Remembering previous diary entries

<!--
* Remembering Wisdom
  * Quotes (for example by the Buddha)
  * Remembering other perceptions (about questions or tags)
    * Ex: People may be having problems with someone, but remember a different perception of this person, it's useful to be reminded of this alternative perception by the application
    * 
-->



## Scenarios

* User does a small kind action (like sending a song to a friend or family member) and want to make this into a habit, or to remember to do this more often
* User opens the application and is reminded of the questions, which helps her remember a positive structure
* User wants to see what happened yesterday
* User wants to see what happened today
* 
* User wants to remember some important things
  * old nice posts related to a person

### Scenario A

1. User starts the application
2. (doesn't remember all the experiences of the day)
3. The user is reminded by a question and writes
4. Reminded by an activity and writes
5. Remembers an event and writes
6. 


### Scenario: Reading

User wants to **read**:
* a list of things she is grateful for (maybe with a need)
* how she has contributed to the well-being of others
* how she has managed to cultivate her mind
* an insight she has had during the day
  * and how she has put these into practice in her daily life

User wants to write about how she has been feeling

User wants to highlight an entry making it easier to find it later on

Well-being, happiness and solidity of the user: how this effects the well-being of others

#### Remembering

Window with notification for highly rated entries (and maybe left-side things as well). The user gets these options:
* Close and show again
* Close
* more?

### Wisdoms

Right view, right thinking

Part | Notes
---|---
Seeing monks | Combine with 2 below?
Listening to the Dhamma | Ex: Dharma talks online, books
Remembering the Dhamma | Main/standard, ***Connection to quotes?***
Contemplating the Dhamma | ???
Putting it into practice | **Important**, ***Connection to mind-cultivation?***

### Contributing

Right action, right livelihood, right speech

* User wants to get ideas for how to be of service to others
* User wants to be aware of how she is of service to others by doing what she is already doing

### Gratitude

Right diligence

* Needs
* Feelings?

***Connection to mind cultivation?***

### Mind cultivation

Right diligence

and self-compassion

User wants to *contemplate* wisdoms she has heard from the Dharma and also contemplate her own insights
* From the Dhammapada
* Longer texts
* 

### Activities, friends, places

* User wants to see *ideas and plans* for the coming days
* User would like to have a basic *structure* that she can fall back on
* User wants a list of *activities* she can do, *people* to call, *places* to go (for hiking, mindful walking, etc)
* Same as above, but when she is not feeling well

* List of *gathas* that the user can use to practice in her daily life

Considerations: Do we want to have this in another application? Does it fit together with the other use cases?



## Info architecture

Application information structure (information architecture):
* Diary
  * Gratitude, contribution, mind-cultivation
  * Wisdom in form of quotes
* Plans
  * Activities (Plan)
  * Friends (Plan)
  * Places (Plan)
* Rememberances
  * Gratitudes 38/10
  * Gathas
  * Wisdoms

***


Global reminders
* ex: quotes

Filters for choosing which questions are displayed, ex how i am feeling

Questions are not to be answered but rather as an inspiration to the user to write. Also it provides a few default tags that the user can choose from and that helps her to be mindful

Questions
* description
  * motivation for the action
* Structured
  * **time of day** - for bodily actions
    * optionally sorting by time
* Unstructured
* Days of the week
  * Mo, Tu, We, etc (default is all days) - shortcuts for all weekdays, and for weekends
* Type: Bodily actions, **mental states**
  * Bodily actions ("standard") can be planned
  * Mental - example: "How anxious was i today and how did i deal with it?"
* Archived or active

Wisdom and Insights
* Quotes or saying
  * Person
* Our own experiences
  * Gratitude

Journal Entries
* Bookmark as a good example
* Bookmark as wanting to remember (for example gratitude)
* Time of day
  * either from structure or from added

Local tags

Global tags
* Examples:
  * feelings, needs
  * friends
  * four immeasurable minds
  * contribution
  * gratitude
  * **NEW** encouragement
  * wisdom



## User Requirements

### Starring

Whole entries are starred rather than tags. A few reasons:
* The entries are small enough that the rating is *transferable* to the tags for the entry
* It's easier for the user to type (don't have to add + at the end of the tags)

### Tags

* Each tag is added manually by the user
* Each tag has a display name and a group of search names
  * Ex: Display: Magnus. Search 1: Magnus. Search 2: Maggan
* Adding tags inline or not?
  * we could to this differently depending on whether it is a journal or not
  * one reason to attach to the whole entry rather than adding it inline is the same as above for the starring: the entries are small enough that this makes sense
  * Summary: things like friends, places, mind states, etc can be added inline; and journals can be added to the entry.
    * One challenge with having several journals for an entry is how to display this, it is difficult to do that on the side so maybe best to display a list of the journals below the entry

#### Templates

Friends:
* Needs
* How i can help make him/her happy
* Contact information
* _


### List of activities


## Ideas

GUI idea: combobox as an alternative to the listwidgets

