"""
Module comments:
Global vars are used for storing some of the global application states.
Please don't use global vars for storing other types of values
Also, please collect all global vars in this file
"""

import logging
import enum
import os
import datetime
import io
import typing
from PyQt5 import QtCore
import PIL.Image
import PIL.ExifTags


# Directory and file names
USER_FILES_DIR_STR = "user_files"
IMAGES_DIR_STR = "thumbnail_images"
DATABASE_FILE_STR = "wbd_database.sqlite"
ICONS_DIR_STR = "icons"
BACKUP_DIR_STR = "backups"
LOGS_DIR_STR = "logs"
PUBLIC_DIR_STR = "public"

db_file_exists_at_application_startup_bl = False
testing_bool = False
debugging_bool = True

WBD_APPLICATION_VERSION_STR = "prototype 5"
WBD_APPLICATION_NAME_STR = "Well-Being Diary"
NO_ACTIVE_QUESTION_INT = -1
NO_ACTIVE_FILTER_PRESET_INT = -1
NO_ACTIVE_TAG_INT = -1
NO_DIARY_ENTRY_EDITING_INT = -1
NO_VIEW_ACTIVE_INT = -1
DATETIME_NOT_SET_STR = ""
QT_NO_ROW_SELECTED_INT = -1


class EventSource(enum.Enum):
    application_start = 0
    page_changed = 1
    filters_changed = 2
    question_activated = 3
    tags_changed = 4
    entry_edit = 5
    importing = 6
    diary_view_activated = 7
    entry_area_close = 8
    image_import = 9
    view_changed = 11
    view_tags_tag_changed = 12
    all_tags_tag_changed = 13
    tag_deleted = 14
    tag_added = 15
    add_tag_to_view = 16
    remove_tag_from_view = 17
    tag_edited = 18
    all_tags_sort_type_changed = 19
    view_added = 20


class SortType(enum.Enum):
    sort_by_custom_order = -1
    sort_by_name = 0
    sort_by_frequency = 1
    sort_by_time = 2


class Filters:
    def __init__(self):
        self.tag_active_bool = False
        self.tag_id_int = NO_ACTIVE_TAG_INT
        self.search_active_bool = False
        self.search_term_str = ""
        self.rating_active_bool = True
        self.rating_int = 1
        self.datetime_active_bool = False
        self.start_datetime_string = DATETIME_NOT_SET_STR
        self.end_datetime_string = DATETIME_NOT_SET_STR


class ApplicationState:
    def __init__(self):
        self.current_page_number_int = 1
        self.filters = Filters()
        self.question_id: int = NO_ACTIVE_QUESTION_INT
        self.edit_entry_id_list: list = []
        self.view_id: int = NO_VIEW_ACTIVE_INT
        self.tag_id: int = NO_ACTIVE_TAG_INT

    def add_entry(self, new_entry_id: int) -> None:
        if new_entry_id not in self.edit_entry_id_list:
            self.edit_entry_id_list.append(new_entry_id)

    def last_entry(self):
        if len(self.edit_entry_id_list) > 0:
            return self.edit_entry_id_list[-1]
        else:
            return False

    def is_entries_empty(self) -> bool:
        if len(self.edit_entry_id_list) > 0:
            return False
        else:
            return True

    def clear_entries(self):
        self.edit_entry_id_list.clear()

    def remove_last_entry(self) -> bool:
        if len(self.edit_entry_id_list) > 0:
            del self.edit_entry_id_list[-1]
            return True
        else:
            return False


active_state = ApplicationState()


# active_filter = Filters()

# Image related constants
ORIENTATION_EXIF_TAG_NAME_STR = "Orientation"
DATETIME_ORIGINAL_EXIF_TAG_NAME_STR = "DateTimeOriginal"
SIZE_TE = (512, 512)
JPEG_FORMAT_STR = "JPEG"

DIARY_ENTRIES_PER_PAGE_INT = 10

# Datetime formats
# Python: https://docs.python.org/3/library/datetime.html#datetime.datetime.isoformat
# SQLite: https://www.sqlite.org/lang_datefunc.html
# Qt: http://doc.qt.io/qt-5/qdatetime.html#fromString-1
# Camera EXIF: https://www.awaresystems.be/imaging/tiff/tifftags/privateifd/exif/datetimeoriginal.html
PY_DATETIME_FORMAT_STR = "%Y-%m-%dT%H:%M:%S"
PY_DATETIME_FILENAME_FORMAT_STR = "%Y-%m-%d_%H-%M-%S"
QT_EXIF_DATETIME_FORMAT_STR = "yyyy:MM:dd HH:mm:ss"
# -documentation: "YYYY:MM:DD HH:MM:SS" - please note the colons instead of dashes in the date
PY_DATE_ONLY_FORMAT_STR = "%Y-%m-%d"
QT_DATETIME_FORMAT_STR = "yyyy-MM-ddTHH:mm:ss"
QT_DATE_ONLY_FORMAT_STR = "yyyy-MM-dd"


class MoveDirectionEnum(enum.Enum):
    up = 1
    down = 2


def get_base_dir() -> str:
    first_str = os.path.abspath(__file__)
    # -__file__ is the file that started the application, in other words mindfulness-at-the-computer.py
    second_str = os.path.dirname(first_str)
    base_dir_str = os.path.dirname(second_str)
    return base_dir_str


def get_icon_path(i_file_name: str) -> str:
    ret_icon_path_str = os.path.join(get_base_dir(), ICONS_DIR_STR, i_file_name)
    return ret_icon_path_str


def get_database_filename(i_backup_timestamp: str = "") -> str:
    if testing_bool:
        return ":memory:"
    else:
        database_filename_str = DATABASE_FILE_STR
        if i_backup_timestamp:
            database_filename_str = i_backup_timestamp + "_" + DATABASE_FILE_STR
        ret_path_str = os.path.join(
            get_base_dir(),
            USER_FILES_DIR_STR,
            database_filename_str
        )
        return ret_path_str


def get_user_logs_path(i_file_name: str = "") -> str:
    if i_file_name:
        log_files_path_str = os.path.join(get_base_dir(), LOGS_DIR_STR, i_file_name)
    else:
        log_files_path_str = os.path.join(get_base_dir(), LOGS_DIR_STR)
    os.makedirs(os.path.dirname(log_files_path_str), exist_ok=True)
    return log_files_path_str


def get_public_path(i_file_name: str = "") -> str:
    if i_file_name:
        public_files_path_str = os.path.join(get_base_dir(), PUBLIC_DIR_STR, i_file_name)
    else:
        public_files_path_str = os.path.join(get_base_dir(), PUBLIC_DIR_STR)
    os.makedirs(os.path.dirname(public_files_path_str), exist_ok=True)
    return public_files_path_str


def get_user_backup_path(i_file_name: str = "") -> str:
    if i_file_name:
        user_files_path_str = os.path.join(get_base_dir(), USER_FILES_DIR_STR, BACKUP_DIR_STR, i_file_name)
    else:
        user_files_path_str = os.path.join(get_base_dir(), USER_FILES_DIR_STR, BACKUP_DIR_STR)
    return user_files_path_str
    # user_dir_path_str = QtCore.QDir.currentPath() + "/user_files/images/"
    # return QtCore.QDir.toNativeSeparators(user_dir_path_str)


def get_testing_images_path(i_file_name: str="") -> str:
    if i_file_name:
        testing_images_path_str = os.path.join(get_base_dir(), "varia", "unprocessed_image_files_for_testing", i_file_name)
    else:
        testing_images_path_str = os.path.join(get_base_dir(), "varia", "unprocessed_image_files_for_testing")
    return testing_images_path_str


def process_image(i_file_path: str) -> (bytes, QtCore.QDateTime):
    image_pi: PIL.Image = PIL.Image.open(i_file_path)

    # Time when the photo was taken
    time_photo_taken_qdatetime = get_datetime_image_taken(image_pi)
    # -important that this is done before rotating since rotating removes exif data

    # Rotating
    rotation_degrees_int = get_rotation_degrees(image_pi)
    if rotation_degrees_int != 0:
        image_pi = image_pi.rotate(rotation_degrees_int, expand=True)
        # -Warning: Rotating removes exif data (unknown why)

    image_pi = image_pi.convert('RGB')
    # -TODO: Only if needed, or is nothing done in the convert function if the image already is RGB?
    #  How to check is described in this answer: https://stackoverflow.com/a/43259180/2525237
    image_pi.thumbnail(SIZE_TE, PIL.Image.ANTIALIAS)
    # -Please note that exif metadata is removed
    #  If we want to keep exif metadata: https://stackoverflow.com/q/17042602/2525237

    image_byte_stream = io.BytesIO()
    image_pi.save(image_byte_stream, format=JPEG_FORMAT_STR)
    image_bytes = image_byte_stream.getvalue()

    return (image_bytes, time_photo_taken_qdatetime)


def get_rotation_degrees(i_image_pi: PIL.Image, i_switch_direction: bool=False) -> int:
    # Inspiration for this function:
    # https://stackoverflow.com/questions/4228530/pil-thumbnail-is-rotating-my-image
    # https://coderwall.com/p/nax6gg/fix-jpeg-s-unexpectedly-rotating-when-saved-with-pil
    ret_degrees_int = 0

    orientation_tag_key_str = ""
    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == ORIENTATION_EXIF_TAG_NAME_STR:
            orientation_tag_key_str = tag_key_str
            break
    if orientation_tag_key_str == "":
        logging.warning("get_rotation_degrees - exif tag not found")

    ret_degrees_int = 0
    try:
        exif_data_dict = dict(i_image_pi._getexif().items())
        if exif_data_dict[orientation_tag_key_str] == 3:
            ret_degrees_int = 180
        elif exif_data_dict[orientation_tag_key_str] == 6:
            ret_degrees_int = 270
        elif exif_data_dict[orientation_tag_key_str] == 8:
            ret_degrees_int = 90
        if i_switch_direction:
            ret_degrees_int = -ret_degrees_int
    except AttributeError:
        # -A strange problem: If we use hasattr(i_image_pi, "_getexif") this will return True,
        #  so instead we use this exception handling
        logging.warning(
            "get_rotation_degrees - Image doesn't have exif data. This may be because it has already been processed by an application"
        )

    return ret_degrees_int


def get_datetime_image_taken(i_image_pi: PIL.Image) -> QtCore.QDateTime:
    # Please note that usually (always?) the time we get from the carmera is in the UTC time zone:
    # https://photo.stackexchange.com/questions/82166/is-it-possible-to-get-the-time-a-photo-was-taken-timezone-aware
    # So we need to convert the time that we get
    ret_datetime_qdt = None
    datetime_original_tag_key_str = ""
    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == DATETIME_ORIGINAL_EXIF_TAG_NAME_STR:
            datetime_original_tag_key_str = tag_key_str
            break
    if datetime_original_tag_key_str == "":
        logging.warning("get_datetime_image_taken - exif tag not found")
    try:
        exif_data_dict = dict(i_image_pi._getexif().items())
        # -Good to be aware that _getexif() is an experimental function:
        #  https://stackoverflow.com/a/48428533/2525237
        datetime_exif_string = exif_data_dict[datetime_original_tag_key_str]
        logging.debug("datetime_exif_string = " + datetime_exif_string)
        from_camera_qdt = QtCore.QDateTime.fromString(datetime_exif_string, QT_EXIF_DATETIME_FORMAT_STR)
        from_camera_qdt.setTimeSpec(QtCore.Qt.UTC)
        ret_datetime_qdt = from_camera_qdt.toLocalTime()
        logging.debug("from_camera_qdt.toString = " + ret_datetime_qdt.toString(QT_DATETIME_FORMAT_STR))
    except AttributeError:
        # -A strange problem: If we use hasattr(i_image_pi, "_getexif") this will return True,
        #  so instead we use this exception handling
        logging.warning("get_datetime_image_taken - Image doesn't have exif data. This may be because it has already been processed by an application")

    return ret_datetime_qdt


def process_and_get_image_file_path_from_bytes(i_image: bytes) -> str:
    image_byte_stream = io.BytesIO(i_image)
    image_pillow = PIL.Image.open(image_byte_stream)
    file_path_str = "filename.jpg"
    image_pillow.save(file_path_str)
    return file_path_str


def get_today_datetime_string() -> str:
    now_pdt = datetime.datetime.now()
    time_as_iso_str = now_pdt.strftime(PY_DATE_ONLY_FORMAT_STR)
    return time_as_iso_str


def get_now_datetime_string() -> str:
    now_pdt = datetime.datetime.now()
    time_as_iso_str = now_pdt.strftime(PY_DATETIME_FORMAT_STR)
    return time_as_iso_str


def clear_widget_and_layout_children(qlayout_or_qwidget):
    if qlayout_or_qwidget.widget():
        qlayout_or_qwidget.widget().deleteLater()
    elif qlayout_or_qwidget.layout():
        while qlayout_or_qwidget.layout().count():
            child_qlayoutitem = qlayout_or_qwidget.takeAt(0)
            clear_widget_and_layout_children(child_qlayoutitem)  # Recursive call

