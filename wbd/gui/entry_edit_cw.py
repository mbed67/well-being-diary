import logging
import datetime
import wbd.gui.diary_entries_cw
import wbd.model
import os
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import QtGui
import wbd.wbd_global
import wbd.gui.image_selection_dlg

WIDTH_AND_HEIGHT_INT = 180


class EntryEditCw(QtWidgets.QWidget):
    close_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()

        """
        self.setSizePolicy(
            self.sizePolicy().horizontalPolicy(),
            QtWidgets.QSizePolicy.Maximum
        )
        """

        self.updating_gui_bool = False

        self.vbox_l2 = QtWidgets.QVBoxLayout()
        self.setLayout(self.vbox_l2)

        # Row 1 (question)
        self.question_qll = QtWidgets.QLabel()
        self.question_qll.setWordWrap(True)
        new_font = self.question_qll.font()
        new_font.setPointSize(14)
        self.question_qll.setFont(new_font)
        self.vbox_l2.addWidget(self.question_qll)

        # Row 2 (text input area)
        self.diary_entry_cpte = CustomPlainTextEdit()
        new_font = QtGui.QFont()
        new_font.setPointSize(14)
        self.diary_entry_cpte.setFont(new_font)
        self.diary_entry_cpte.setStyleSheet("background-color:#d0e8c9")
        self.diary_entry_cpte.ctrl_plus_enter_signal.connect(self.on_entry_area_ctrl_plus_enter_pressed)
        self.diary_entry_cpte.setSizePolicy(
            self.diary_entry_cpte.sizePolicy().horizontalPolicy(),
            QtWidgets.QSizePolicy.MinimumExpanding
        )
        self.diary_entry_cpte.textChanged.connect(self.on_diary_entry_text_changed)
        self.vbox_l2.addWidget(self.diary_entry_cpte)

        # Row 3 (varia)
        hbox_l3 = QtWidgets.QHBoxLayout()
        self.vbox_l2.addLayout(hbox_l3)

        # ..column 1 (leftmost) (tags)
        self.tags_for_entry_qlw = QtWidgets.QListWidget()
        self.tags_for_entry_qlw.setSelectionMode(QtWidgets.QListView.MultiSelection)
        self.tags_for_entry_qlw.setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        # self.tags_for_entry_qlw.setFixedWidth(140)
        self.tags_for_entry_qlw.itemSelectionChanged.connect(self.on_item_selection_changed)
        hbox_l3.addWidget(self.tags_for_entry_qlw, stretch=4)

        # ..column 2 (image)
        image_vbox_l4 = QtWidgets.QVBoxLayout()
        hbox_l3.addLayout(image_vbox_l4, stretch=4)

        self.set_image_qpb = QtWidgets.QPushButton("Add image")
        self.set_image_qpb.clicked.connect(self.on_set_image_clicked)
        image_vbox_l4.addWidget(self.set_image_qpb)

        self.remove_image_qpb = QtWidgets.QPushButton("Remove image")
        self.remove_image_qpb.clicked.connect(self.on_remove_image_clicked)
        image_vbox_l4.addWidget(self.remove_image_qpb)

        self.image_file_name_qll = QtWidgets.QLabel()
        self.image_file_name_qll.setWordWrap(True)
        image_vbox_l4.addWidget(self.image_file_name_qll)

        self.image_preview_qll = QtWidgets.QLabel()
        image_vbox_l4.addWidget(self.image_preview_qll)
        # self.image_preview_qll

        # ..column 3 (add new buttons, time and date)
        vbox_l4 = QtWidgets.QVBoxLayout()
        hbox_l3.addLayout(vbox_l4, stretch=4)

        self.date_qde = QtWidgets.QDateEdit()
        self.date_qde.setCalendarPopup(True)
        self.date_qde.dateChanged.connect(self.on_date_changed)
        vbox_l4.addWidget(self.date_qde)


        self.all_day_qcb = QtWidgets.QCheckBox("All Day")
        self.all_day_qcb.toggled.connect(self.on_all_day_toggled)
        self.all_day_qcb.setChecked(True)
        vbox_l4.addWidget(self.all_day_qcb)


        self.hour_of_day_qsr = QtWidgets.QSlider()
        self.hour_of_day_qsr.setOrientation(QtCore.Qt.Horizontal)
        self.hour_of_day_qsr.setMinimum(0)
        self.hour_of_day_qsr.setMaximum(23)
        self.hour_of_day_qsr.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.hour_of_day_qsr.setTickInterval(6)
        self.hour_of_day_qsr.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.hour_of_day_qsr.valueChanged.connect(self.on_hour_slider_changed)
        vbox_l4.addWidget(self.hour_of_day_qsr)

        self.minute_qsr = QtWidgets.QSlider()
        self.minute_qsr.setOrientation(QtCore.Qt.Horizontal)
        self.minute_qsr.setMinimum(0)
        self.minute_qsr.setMaximum(11)
        self.minute_qsr.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.minute_qsr.setTickInterval(3)
        self.minute_qsr.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.minute_qsr.valueChanged.connect(self.on_minute_slider_changed)
        vbox_l4.addWidget(self.minute_qsr)

        self.time_of_day_qll = QtWidgets.QLabel()
        vbox_l4.addWidget(self.time_of_day_qll)


        now_and_yesterday_hbox_l5 = QtWidgets.QHBoxLayout()
        vbox_l4.addLayout(now_and_yesterday_hbox_l5)

        self.now_qpb = QtWidgets.QPushButton("Now")
        self.now_qpb.clicked.connect(self.on_now_clicked)
        now_and_yesterday_hbox_l5.addWidget(self.now_qpb)

        self.yesterday_qpb = QtWidgets.QPushButton("Yesterday")
        self.yesterday_qpb.clicked.connect(self.on_yesterday_clicked)
        now_and_yesterday_hbox_l5.addWidget(self.yesterday_qpb)

        self.rating_qbuttongroup = QtWidgets.QButtonGroup(self)
        # self.rating_qbuttongroup.buttonToggled.connect(self.on_rating_toggled)
        rating_hbox_l5 = QtWidgets.QHBoxLayout()
        vbox_l4.addLayout(rating_hbox_l5)
        self.rating_zero_qrb = QtWidgets.QRadioButton("0")
        self.rating_qbuttongroup.addButton(self.rating_zero_qrb, 0)
        rating_hbox_l5.addWidget(self.rating_zero_qrb)
        self.rating_one_qrb = QtWidgets.QRadioButton("1")
        self.rating_qbuttongroup.addButton(self.rating_one_qrb, 1)
        rating_hbox_l5.addWidget(self.rating_one_qrb)
        self.rating_two_qrb = QtWidgets.QRadioButton("2")
        self.rating_qbuttongroup.addButton(self.rating_two_qrb, 2)
        rating_hbox_l5.addWidget(self.rating_two_qrb)
        self.rating_three_qrb = QtWidgets.QRadioButton("3")
        self.rating_qbuttongroup.addButton(self.rating_three_qrb, 3)
        rating_hbox_l5.addWidget(self.rating_three_qrb)
        self.rating_one_qrb.setChecked(True)
        self.rating_qbuttongroup.buttonClicked.connect(self.on_rating_button_clicked)

        self.close_qpb = QtWidgets.QPushButton("Close")
        self.close_qpb.setFixedHeight(40)
        self.close_qpb.clicked.connect(self.on_close_clicked)
        vbox_l4.addWidget(self.close_qpb)

        # self.reinitiate()

    # Overridden
    def closeEvent(self, i_QCloseEvent):
        logging.debug("entry_edit_cw: closeEvent")

    def on_all_day_toggled(self):
        if self.updating_gui_bool or wbd.wbd_global.active_state.is_entries_empty():
            return
        self.update_db_datetime()
        self.update_gui()
        """        
        wbd.model.update_entry_all_day(
            wbd.wbd_global.active_diary_entry_id,
            self.all_day_qcb.isChecked()
        )
        # self.update_gui()
        """

    def on_entry_area_ctrl_plus_enter_pressed(self):
        self.close_signal.emit()

    def on_remove_image_clicked(self):
        if self.updating_gui_bool:
            return
        wbd.model.EntryM.update_image(wbd.wbd_global.active_state.last_entry(), None)
        self.update_gui()

    def on_set_image_clicked(self):
        if self.updating_gui_bool:
            return
        file_path_str = wbd.gui.image_selection_dlg.ImageFileDialog.open_dlg_and_get_image_path()
        if file_path_str:
            (image_file_bytes, image_taken_qdatetime) = wbd.wbd_global.process_image(file_path_str)
            wbd.model.EntryM.update_image(wbd.wbd_global.active_state.last_entry(), image_file_bytes)
            if image_taken_qdatetime is not None:
                wbd.model.EntryM.update_datetime(
                    wbd.wbd_global.active_state.last_entry(),
                    image_taken_qdatetime.toString(wbd.wbd_global.QT_DATETIME_FORMAT_STR)
                )
        self.update_gui()

    def add_active_tag(self):
        if wbd.wbd_global.active_state.tag_id == wbd.wbd_global.NO_ACTIVE_TAG_INT:
            return
        active_tag = wbd.model.TagM.get(wbd.wbd_global.active_state.tag_id)
        self.tags_for_entry_qlw.addItem(active_tag.title_str)
        self.select_last_row()

    def select_last_row(self):
        last_row_int = self.tags_for_entry_qlw.count() - 1
        list_item: QtWidgets.QListWidgetItem = self.tags_for_entry_qlw.item(last_row_int)
        list_item.setSelected(True)

    def get_datetime_string_from_widgets(self) -> str:
        qdatetime = QtCore.QDateTime()
        qdatetime.setDate(self.date_qde.date())

        if self.all_day_qcb.isChecked():
            ret_string = qdatetime.toString(wbd.wbd_global.QT_DATE_ONLY_FORMAT_STR)
        else:
            qtime = QtCore.QTime()
            hour_int = self.hour_of_day_qsr.value()
            minute_int = self.get_minute_from_slider()
            qtime.setHMS(hour_int, minute_int, 0)
            qdatetime.setTime(qtime)
            ret_string = qdatetime.toString(wbd.wbd_global.QT_DATETIME_FORMAT_STR)

        return ret_string

    def on_now_clicked(self):
        self.update_times()

    def on_yesterday_clicked(self):
        self.update_times(True)

    def update_times(self, i_yesterday: bool=False):
        qdatetime = QtCore.QDateTime.currentDateTime()
        if i_yesterday:
            qdatetime = qdatetime.addDays(-1)
        active_qtime: QtCore.QTime = qdatetime.time()
        active_qdate: QtCore.QDate = qdatetime.date()
        # self.hour_of_day_qsr.setValue(active_qtime.hour())
        # -the label will be automatically updated when setValue is called
        self.date_qde.setDate(active_qdate)

    def on_hour_slider_changed(self, i_value: int):
        if self.updating_gui_bool:
            return
        # qtime = QtCore.QTime(i_value, 0)

        self.all_day_qcb.setChecked(False)
        # self.time_of_day_qte.setTime(qtime)
        self.update_db_datetime()
        self.update_gui()

    def on_minute_slider_changed(self, i_value: int):
        if self.updating_gui_bool:
            return
        # qtime = QtCore.QTime(i_value, 0)

        self.all_day_qcb.setChecked(False)
        # self.time_of_day_qte.setTime(qtime)
        self.update_db_datetime()
        self.update_gui()

    def get_minute_from_slider(self) -> int:
        return self.minute_qsr.value() * 5

    """
    def reinitiate(self, i_new_entry: bool=False):
        if wbd.wbd_global.active_diary_entry_id == wbd.wbd_global.NO_DIARY_ENTRY_EDITING_INT:
            return
        diary_entry = wbd.model.EntryM.get_entry(wbd.wbd_global.active_diary_entry_id)
        wbd.model.EntryM.update_entry_image(wbd.wbd_global.active_diary_entry_id, diary_entry.image_file_name_str)
        # if i_focus_on_text_area:
        self.diary_entry_cpte.setFocus()
        if i_new_entry:
            self.now_qpb.click()
            self.rating_one_qrb.setChecked(True)
    """

    def on_close_clicked(self):
        self.close_signal.emit()

    def on_diary_entry_text_changed(self):
        if self.updating_gui_bool:
            return
        notes_sg = self.diary_entry_cpte.toPlainText().strip()
        print("notes_sg = " + notes_sg)
        wbd.model.EntryM.update_text(wbd.wbd_global.active_state.last_entry(), notes_sg)

    def on_rating_button_clicked(self):
        if self.updating_gui_bool:
            return
        selected_rating_int = self.rating_qbuttongroup.checkedId()
        wbd.model.EntryM.update_rating(wbd.wbd_global.active_state.last_entry(), selected_rating_int)

    def on_date_changed(self):
        if self.updating_gui_bool:
            return
        self.update_db_datetime()

    def update_db_datetime(self):
        datetime_str = self.get_datetime_string_from_widgets()
        logging.debug("qdatetime = " + datetime_str)
        wbd.model.EntryM.update_datetime(
            wbd.wbd_global.active_state.last_entry(),
            datetime_str
        )

    def on_item_selection_changed(self):
        if self.updating_gui_bool:
            return
        entry_id_int = wbd.wbd_global.active_state.last_entry()
        wbd.model.remove_all_tag_relations_for_entry(entry_id_int)
        for selected_tag_item in self.tags_for_entry_qlw.selectedItems():
            tag_name_str = selected_tag_item.text()
            tag = wbd.model.TagM.get_by_title(tag_name_str)
            tag_id_int = tag.id_int
            logging.debug("tag.id_int = " + str(tag.id_int))
            wbd.model.add_tag_entry_relation(tag_id_int, entry_id_int)
            logging.debug("Relation between tag " + str(tag_id_int) + " and entry " + str(entry_id_int) + " added")

    def update_gui(self):
        self.updating_gui_bool = True

        if wbd.wbd_global.active_state.is_entries_empty():
            logging.error("wbd.wbd_global.active_diary_entry_id is not set in update_gui function")

        diary_entry: wbd.model.EntryM = wbd.model.EntryM.get(wbd.wbd_global.active_state.last_entry())

        # Question text
        if wbd.wbd_global.active_state.question_id == wbd.wbd_global.NO_ACTIVE_QUESTION_INT:
            self.question_qll.hide()
        else:
            self.question_qll.show()
            question = wbd.model.QuestionM.get(wbd.wbd_global.active_state.question_id)
            self.question_qll.setText(question.description_str)

        # Entry area
        self.diary_entry_cpte.setPlainText(diary_entry.diary_text_str)

        # Tags
        self.tags_for_entry_qlw.clear()
        # if wbd.wbd_global.active_question_id_int != wbd.wbd_global.NO_ACTIVE_QUESTION_INT:
        if wbd.wbd_global.active_state.question_id != wbd.wbd_global.NO_ACTIVE_QUESTION_INT:
            tags_and_preselected_list = wbd.model.get_all_tags_referenced_by_question(wbd.wbd_global.active_state.question_id)
            for (tag, preselected_bool) in tags_and_preselected_list:
                self.tags_for_entry_qlw.addItem(tag.title_str)
                if preselected_bool:
                    self.select_last_row()
        else:
            tags_list = wbd.model.get_all_tags_referenced_by_entry(wbd.wbd_global.active_state.last_entry())
            for tag in tags_list:
                self.tags_for_entry_qlw.addItem(tag.title_str)
                self.select_last_row()  # -selecting all rows

        # Image
        if diary_entry.image_file_bytes is not None:
            # self.image_file_name_qll.setText(diary_entry.image_file_bytes)
            qpixmap = QtGui.QPixmap()
            qpixmap.loadFromData(diary_entry.image_file_bytes)
            # -documentation: http://doc.qt.io/qt-5/qpixmap.html#loadFromData-1
            scaled_qpixmap = qpixmap.scaled(
                WIDTH_AND_HEIGHT_INT,
                WIDTH_AND_HEIGHT_INT,
                QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation
            )
            self.image_preview_qll.setPixmap(scaled_qpixmap)
        else:
            self.image_file_name_qll.setText("no image")
            self.image_preview_qll.setText("<preview area>")

        # if wbd.wbd_global.active_diary_entry_id == wbd.wbd_global.NO_DIARY_ENTRY_EDITING_INT:
        # else:

        # Date
        """
        date_part_int = diary_entry.date_added_it // 86400  # -will be rounded down
        time_part_int = diary_entry.date_added_it % 86400
        """

        # diary_entry.datetime_added_str

        if diary_entry.is_all_day():
            qdate = QtCore.QDate.fromString(
                diary_entry.datetime_added_str,
                wbd.wbd_global.QT_DATE_ONLY_FORMAT_STR
            )
            self.date_qde.setDate(qdate)
            self.hour_of_day_qsr.setValue(0)
            self.minute_qsr.setValue(0)
            self.time_of_day_qll.setText("---- all day ----")
        else:
            qdatetime = QtCore.QDateTime.fromString(
                diary_entry.datetime_added_str,
                wbd.wbd_global.QT_DATETIME_FORMAT_STR
            )
            self.date_qde.setDate(qdatetime.date())

            hour_int = qdatetime.time().hour()
            self.hour_of_day_qsr.setValue(hour_int)

            minute_int = qdatetime.time().minute()
            self.minute_qsr.setValue(minute_int // 5)

            hour_formatted_str = str(hour_int).zfill(2) + ":" + str(minute_int).zfill(2)
            self.time_of_day_qll.setText(hour_formatted_str)

        self.all_day_qcb.setChecked(diary_entry.is_all_day())
        # self.time_of_day_qte.setTime(qdatetime.time())

        # Rating
        if diary_entry.rating_int == 0:
            self.rating_zero_qrb.click()
        elif diary_entry.rating_int == 1:
            self.rating_one_qrb.click()
        elif diary_entry.rating_int == 2:
            self.rating_two_qrb.click()
        elif diary_entry.rating_int == 3:
            self.rating_three_qrb.click()

        """
        # Button text
        if wbd.wbd_global.active_diary_entry_id == wbd.wbd_global.NO_DIARY_ENTRY_EDITING_INT:
            self.save_qpb.setText(ADD_DIARY_BN_TEXT_STR)
            # self.cancel_editing_qbn_w3.hide()
        else:
            self.save_qpb.setText(EDIT_DIARY_BN_TEXT_STR)
            # self.cancel_editing_qbn_w3.show()
        """

        self.updating_gui_bool = False


class CustomPlainTextEdit(QtWidgets.QPlainTextEdit):
    ctrl_plus_enter_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()

    # Overridden
    def keyPressEvent(self, i_qkeyevent):
        if QtWidgets.QApplication.keyboardModifiers() == QtCore.Qt.ControlModifier:
            if i_qkeyevent.key() == QtCore.Qt.Key_Enter or i_qkeyevent.key() == QtCore.Qt.Key_Return:
                logging.debug("CtrlModifier + Enter/Return")
                self.ctrl_plus_enter_signal.emit()
                return
        else: # -no keyboard modifier
            if i_qkeyevent.key() == QtCore.Qt.Key_Enter or i_qkeyevent.key() == QtCore.Qt.Key_Return:
                # -http://doc.qt.io/qt-5/qguiapplication.html#keyboardModifiers
                # -Please note that the modifiers are placed directly in the QtCore.Qt namespace
                # Alternatively:
                # if QtWidgets.QApplication.keyboardModifiers() == QtCore.Qt.ShiftModifier:
                # -using bitwise and to find out if the shift key is pressed
                logging.debug("enter or return key pressed in textedit area")
                # self.ref_central.save_entry()
                # return

        QtWidgets.QPlainTextEdit.keyPressEvent(self, i_qkeyevent)
        # -if we get here it means that the key has not been captured elsewhere (or possibly
        # (that the key has been captured but that we want "double handling" of the key event)
