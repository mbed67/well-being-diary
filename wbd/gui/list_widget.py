import logging
import enum
import sys
import typing
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import QtGui
import wbd.wbd_global

NO_POS_SET_INT = -1
NO_ITEM_ACTIVE_INT = -1


class MoveDirectionEnum(enum.Enum):
    up = 1
    down = 2


class ListWidget(QtWidgets.QListWidget):
    """
    External functions:
    update_db_sort_order_for_all_rows

    PLEASE NOTE: Instead of updating the "active row/tag/question/_" we can send the id with the signal

    get_single_item_func takes an int as an argument
    get_all_items_func (takes no arguments)
    update_db_sort_order_func (takes two arguments: id, and new position)

    Limitations:
    * Only a single item can be selected at a time

    Interface:
    .id_int
    .sort_order_int
    .title_str
    """
    ############drop_signal = QtCore.pyqtSignal()
    current_row_changed_signal = QtCore.pyqtSignal(int)

    def __init__(self, i_model_class):
        super().__init__()

        ###########self.get_all_items_func = i_get_all_items_func
        # -the reason for not including this here is that we put it in the update/populate
        #  method instead, since we then can give new values for the list from the outside
        #  for example for the get_tags_referenced_by_view function
        #####self.get_single_item_func = i_get_single_item_func
        #####self.update_db_sort_order_func = i_update_db_sort_order_func
        # -both these are needed for changing the sort order
        self.model_class = i_model_class

        #####if i_update_db_sort_order_func is not None:

        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        # self.list_qlw.setDragEnabled(True)

        ### self.drop_signal.connect(self.update_db_sort_order_for_all_rows)
        self.currentRowChanged.connect(self.on_current_row_changed)

        ########self.populate_list()

    # overridden
    def dropEvent(self, QDropEvent):
        super().dropEvent(QDropEvent)
        self.update_db_sort_order_for_all_rows()
        ######self.drop_signal.emit()
        # self.update_db_sort_order_for_all_rows()

    def move_to_top(self):
        current_row_number_int = self.currentRow()
        self.update_row_item(current_row_number_int, 0)
        self.update_db_sort_order_for_all_rows()

    def move_item_up(self):
        self.move_current_row_up_down(MoveDirectionEnum.up)

    def move_item_down(self):
        self.move_current_row_up_down(MoveDirectionEnum.down)

    def move_current_row_up_down(self, i_move_direction: MoveDirectionEnum) -> None:
        current_row_number_int = self.currentRow()
        position_int = NO_POS_SET_INT
        if i_move_direction == MoveDirectionEnum.up:
            if current_row_number_int >= 0:
                position_int = current_row_number_int - 1
        elif i_move_direction == MoveDirectionEnum.down:
            if current_row_number_int < self.count():
                position_int = current_row_number_int + 1
        if position_int != NO_POS_SET_INT:
            current_row_number_int = self.currentRow()
            self.update_row_item(current_row_number_int, position_int)
            self.update_db_sort_order_for_all_rows()

    def on_current_row_changed(self):
        current_row_int = self.currentRow()
        if current_row_int == wbd.wbd_global.QT_NO_ROW_SELECTED_INT:
            return
        current_item_qli = self.item(current_row_int)
        #########custom_qlabel_widget: CustomQLabel = self.itemWidget(current_item_qli)

        ######### if custom_qlabel_widget is not None:
        #############wbd.wbd_global.active_tag_id_int = custom_qlabel_widget.id_int
        id_int = current_item_qli.data(QtCore.Qt.UserRole)
        self.current_row_changed_signal.emit(id_int)

        logging.debug("self.viewportSizeHint().height() = " + str(self.viewportSizeHint().height()))
        logging.debug("self.sizeHintForRow(current_item_qli) = " + str(self.sizeHintForRow(current_row_int)))

    def update_row_item(self, i_start_pos: int, i_end_pos: int):
        current_list_widget_item: QtWidgets.QListWidgetItem = self.item(i_start_pos)
        id_int = current_list_widget_item.data(QtCore.Qt.UserRole)
        ############item_widget_cql: CustomQLabel = self.itemWidget(current_list_widget_item)
        self.takeItem(i_start_pos)
        # -IMPORTANT: item is removed from list only after the item widget has been extracted.
        #  The reason for this is that if we take the item away from the list the associated
        #  widget (in our case a CustomLabel) will not come with us (which makes sense
        #  if the widget is stored in the list somehow)
        ######model_item = self.get_single_item_func(id_int)
        model_item = self.model_class.get(id_int)  # -duck typing
        item_label_qll = QtWidgets.QLabel(model_item.title_str)  # -duck typing
        row_item = QtWidgets.QListWidgetItem()
        self.insertItem(i_end_pos, row_item)
        self.setItemWidget(row_item, item_label_qll)
        self.setCurrentRow(i_end_pos)

    def update_db_sort_order_for_all_rows(self):
        logging.debug("update_db_sort_order_for_all_rows")
        i = 0
        while i < self.count():
            q_list_item_widget: QtWidgets.QListWidgetItem = self.item(i)
            id_int = q_list_item_widget.data(QtCore.Qt.UserRole)
            row_int = self.row(q_list_item_widget)
            self.model_class.update_sort_order(id_int, row_int)
            logging.debug("id_int = " + str(id_int) + ", row_int = " + str(row_int))
            i += 1

    """
    
        def update_db_sort_order_for_all_rows(self):
        logging.debug("update_db_sort_order_for_all_rows")
        i = 0
        while i < self.list_clw.count():
            q_list_item_widget = self.list_clw.item(i)
            custom_label: CustomQLabel = self.list_clw.itemWidget(q_list_item_widget)
            id_int = custom_label.id_int
            row_int = self.list_clw.row(q_list_item_widget)
            wbd.model.QuestionM.update_sort_order(id_int, row_int)
            logging.debug("id_int = " + str(id_int) + ", row_int = " + str(row_int))
            i += 1

    """

    def update_gui(self):
        # , i_item_list: typing.List
        item_list = self.model_class.get_all()  # -duck typing
        self.clear()
        for model_item in item_list:
            tag_qll = QtWidgets.QLabel(model_item.title_str)
            row_item = QtWidgets.QListWidgetItem()
            row_item.setData(QtCore.Qt.UserRole, model_item.id_int)
            self.addItem(row_item)
            self.setItemWidget(row_item, tag_qll)

        logging.debug("self.viewportSizeHint().height() = " + str(self.viewportSizeHint().height()))

    """
    def unused_populate_list(self):
        self.clear()
        for model_item in self.get_all_items_func():
            tag_qll = QtWidgets.QLabel(model_item.title_str)
            row_item = QtWidgets.QListWidgetItem()
            row_item.setData(QtCore.Qt.UserRole, model_item.id_int)
            self.addItem(row_item)
            self.setItemWidget(row_item, tag_qll)

        logging.debug("self.viewportSizeHint().height() = " + str(self.viewportSizeHint().height()))
    """


app = QtWidgets.QApplication(sys.argv)


class ItemExperimental:
    def __init__(self, i_id: int, i_title: str, i_sort_order: int=0):
        self.id_int = i_id
        self.title_str = i_title
        self.sort_order_int = i_sort_order

list_of_items = [ItemExperimental(1, "First"), ItemExperimental(2, "2nd"), ItemExperimental(3, "3rd")]

def get_single_experimental(i_id: int):
    return list_of_items[0]

def get_all_experimental():
    return list_of_items

def update_sort_order_experimental(i_id: int, i_sort_order: int):
    pass


class MyMainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.my_list_widget = ListWidget(
            get_single_experimental,
            get_all_experimental,
            update_sort_order_experimental
        )
        self.setCentralWidget(self.my_list_widget)
        self.show()


if __name__ == '__main__':
    main_window = MyMainWindow()
    main_window.show()
    app.exec_()

