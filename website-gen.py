import webbrowser
from string import Template
# Documentation: https://docs.python.org/3/library/string.html#template-strings

TEMPLATE_FILE_STR = "docs/template.html"
SOURCE_INDEX_FILE_STR = "docs/index.content.html"
TARGET_INDEX_FILE_STR = "public/index.html"
SOURCE_USER_GUIDE_FILE_STR = "docs/user_guide.content.html"
TARGET_USER_GUIDE_FILE_STR = "public/user_guide.html"
# CONTENT_STR = ".content"
# SUFFIX_STR = ".html"

with open(TEMPLATE_FILE_STR, "r") as template_file,\
open(SOURCE_INDEX_FILE_STR, "r") as index_content_file,\
open(SOURCE_USER_GUIDE_FILE_STR, "r") as user_guide_content_file:
    template = Template(template_file.read())
    home_str = template.substitute(content=index_content_file.read())
    user_guide_str = template.substitute(content=user_guide_content_file.read())

with open(TARGET_INDEX_FILE_STR, "w+") as index_file,\
open(TARGET_USER_GUIDE_FILE_STR, "w+") as user_guide_file:
    index_file.write(home_str)
    user_guide_file.write(user_guide_str)

webbrowser.open("./public/index.html")

"""

About:
History, more description, about SunyataZero, 

Participate:
Suggest a feature, an improvement
Report a bug

Screenshots

User guide

Features

News

Question and tag ideas


"""